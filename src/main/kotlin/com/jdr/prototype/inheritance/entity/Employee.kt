package com.jdr.prototype.inheritance.entity

import lombok.Data
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

@Data
@Entity
@DiscriminatorValue(value = "EMPLOYEE")
class Employee(name: String?, private val salary: Int) : Person() {
}