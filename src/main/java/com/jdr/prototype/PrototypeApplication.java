package com.jdr.prototype;

import com.jdr.prototype.inheritance.entity.Employee;
import com.jdr.prototype.inheritance.repo.EmployeeRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class PrototypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrototypeApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(EmployeeRepo employeeRepo){
		return (args)-> {
			log.warn("CommandLineRunner");
			employeeRepo.save(new Employee("Em", 1212));
		};
	}

}
