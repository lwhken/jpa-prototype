package com.jdr.prototype.inheritance.repo;

import com.jdr.prototype.inheritance.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
}
