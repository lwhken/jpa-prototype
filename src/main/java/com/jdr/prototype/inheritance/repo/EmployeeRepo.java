package com.jdr.prototype.inheritance.repo;

import com.jdr.prototype.inheritance.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
}
