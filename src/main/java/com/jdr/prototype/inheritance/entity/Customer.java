package com.jdr.prototype.inheritance.entity;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DiscriminatorValue(value = "CUSTOMER")
public class Customer extends Person{

	private int visit;
}
