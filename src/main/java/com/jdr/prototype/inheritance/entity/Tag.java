package com.jdr.prototype.inheritance.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Tag {

	@Id
	@GeneratedValue
	private long id;

	@Version
	private long version;

	private String name;

	@ManyToMany
	private Collection<Person> person;
}
