package com.jdr.prototype.inheritance.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "TYPE")
public abstract class Person {

	@Id
	@GeneratedValue
	private long id;

	@Version
	private long version;

	private String name;

}

